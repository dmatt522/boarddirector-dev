# -*- coding: utf-8 -*-
from django.contrib import admin

from common.forms import TemplateForm
from .models import Feedback, TemplateModel


class TemplateAdmin(admin.ModelAdmin):
    form = TemplateForm

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False


admin.site.register(Feedback)
admin.site.register(TemplateModel, TemplateAdmin)
