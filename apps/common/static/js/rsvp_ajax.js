function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
        }
    }
});

function setRsvpButtonClass(accountID, meetingID, selectedOption) {
    var className = function(btn) { return "bg-" + btn.attr("color-scheme") }
    var spanID = "#rsvp\\." + accountID + "\\." + meetingID;
    rsvpButtons = $(spanID + " div[color-scheme]");
    var selectedButtonID = $(spanID + "\\." + selectedOption).attr("id");
    $.each(rsvpButtons, function() {
        if ($(this).attr("id") == selectedButtonID) {
            $(this).addClass(className($(this)));
        }
        else {
            $(this).removeClass(className($(this)));
        }
    });
}

function postRsvp(accountName, accountID, meetingID, response) {
    var ajaxUrl = "/" + accountName + "/rsvp/";
    var postData = {"meeting": meetingID, "response": response};
    var cb = function(xhr, status) {
        try {
            if (status != "success") {
                throw("AJAX error")
            }
            var statusObj = $.parseJSON(xhr.responseText);
            if ((statusObj.account == accountID) && (statusObj.meeting == meetingID)) {
                setRsvpButtonClass(accountID, meetingID, statusObj.response.toLowerCase());
            }
        }
        catch(err) {
            if (response != null) {
                alert("Unable to set/change RSVP response");
            }
        }
    };
    $.ajax({
        url: ajaxUrl,
        type: "POST",
        data: postData,
        cache: false,
        complete: cb});
}

$(document).ready(function() {
    var rsvpDivs = $(".rsvp-edit");
    $.each(rsvpDivs, function() {
        postRsvp($(this).attr("rsvp-account-name"),
                 $(this).attr("rsvp-account-id"),
                 $(this).attr("rsvp-meeting-id"),
                 null);
    });
});