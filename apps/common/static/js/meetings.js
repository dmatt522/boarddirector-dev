$(document).ready(function() {
    $('.hit-first').click(function (ev) {
        if (ev.target === this) {
            $(this).find('a:first').get(0).click();
        }
    });

    $('.submit-form').change(function() {
        $(this).closest('form').submit();
    });

    $('.submit-form-on-click').click(function() {
        $(this).closest('form').submit();
    })
});
