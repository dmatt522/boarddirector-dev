jQuery(document).ready(function ($) {

    function startChange() {
        var startTime = start.value();

        if (startTime) {
            startTime = new Date(startTime);
            startTime.setMinutes(startTime.getMinutes() + this.options.interval);
            end.min(startTime);
            end.value(startTime);
        }
    }

    //init start timepicker
    var time_start = $("#id_time_start");
    if (!time_start.val())
        time_start.val('8:00 AM');
    var start = time_start.kendoTimePicker({
        change: startChange,
        format: "h:mm tt",
        parseFormats: ["HH:mm"]
    }).data("kendoTimePicker");

    //init end timepicker
    var time_end = $("#id_time_end");
    if (!time_end.val())
        time_end.val('10:00 AM');
    var end = time_end.kendoTimePicker({
        format: "h:mm tt",
        parseFormats: ["HH:mm"]
    }).data("kendoTimePicker");

    //define min/max range
    start.min("6:00 AM");
    start.max("10:00 PM");

    //define min/max range
    end.min("6:00 AM");
    end.max("10:00 PM");

    $("#id_date").kendoDatePicker({
        format: "MMM. dd, yyyy"
    });

    agenda_kendo_init();
    minutes_kendo_init();
    other_docs_kendo_init();

    $('#id_extra_members').selectize({dropdownParent: 'body'});

    var update_hidden_action = function () {
        var $action = $('#id_action');
        if ($action.length) {
            $('#action-hidden-field').val($action.val());
        }
    };

    $('#id_action').change(update_hidden_action);
    update_hidden_action();

    $('.meeting-right-side .create-meeting').click(function () {
        var $prompt = $('#prompt-send-email');
        if ($prompt.length) {
            var kendoWindow = $("<div />").kendoWindow({
                title: gettext('Confirm'),
                resizable: false,
                modal: true,
                height: "110px",
                width: "210px",
                visible: false
            });

            kendoWindow.data("kendoWindow")
                .content($prompt.html())
                .center().open();

            kendoWindow
                .find("button")
                .click(function (e) {
                    e.preventDefault();
                    if ($(this).hasClass("skip-emails")) {
                        $('#action-hidden-field').val('update-no-email');
                    }
                    kendoWindow.data("kendoWindow").close();
                    $('.createmeeting-form').submit();
                })
                .end();
        } else {
            $('.createmeeting-form').submit();
        }
    });
});
