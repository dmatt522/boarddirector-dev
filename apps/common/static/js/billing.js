$(document).ready(function() {
    $("#id_monthpicker").kendoDatePicker({
        start: "year",
        depth: "year",
        format: "MMMM yyyy"
    });

    $("#id_creditcardnumber").mask("9999-9999-9999-999?9");

    $('#what_this').live("click", function (event) {
        event.preventDefault();

        var kendoWindow = $("<div />").kendoWindow({
            title: gettext("What's This?"),
            resizable: false,
            modal: true,
            height: "500px",
            width: "300px",
            visible: false
        });

        kendoWindow.data("kendoWindow")
            .content($("#cvv-information").html())
            .center().open();
    });
});
