# -*- coding: utf-8 -*-
from django.utils import timezone
from django.conf import settings
from django.http import HttpResponsePermanentRedirect


class TimezoneMiddleware(object):

    def process_request(self, request):
        if request.user.is_authenticated() and 'current_account' in request.session:
            membership = request.user.get_membership(request.session['current_account'])
            timezone.activate(membership.timezone)


class ForceSSLMiddleware(object):
    """
    Redirects all (non-DEBUG) requests to go through SSL.

    Picks up the `HTTP_X_FORWARDED_PROTO` proxy header set by Heroku.

    Also sets the "Strict-Transport-Security" header for 600 seconds so that
    compliant browsers force all requests to this domain to use SSL.
    Can be disabled in settings:

        SSL_USE_STS_HEADER = False
    """

    SSL_HEADER = getattr(settings, "SECURE_PROXY_SSL_HEADER", ('', ''))
    HTTP_X_FORWARDED_PROTO, HTTPS = SSL_HEADER

    def process_request(self, request):
        if not any([
            settings.DEBUG,
            request.is_secure(),
            request.META.get(self.HTTP_X_FORWARDED_PROTO, '') == self.HTTPS
        ]):
            return self._redirect(request)

    def process_response(self, request, response):
        return self._response_sts(response)

    def _redirect(self, request):
        if settings.DEBUG and request.method == 'POST':
            raise RuntimeError('Django can\'t perform a SSL redirect while maintaining POST data. '
                               'Please structure your views so that redirects only occur during GETs.')

        url = request.build_absolute_uri(request.get_full_path())
        secure_url = url.replace("http://", "https://")
        return self._response_sts(HttpResponsePermanentRedirect(secure_url))

    def _response_sts(self, response):
        if not getattr(settings, "SSL_USE_STS_HEADER", True):
            return response

        response['Strict-Transport-Security'] = "max-age=600"
        return response
