# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.generic import TemplateView, CreateView, View
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect

from .forms import ContactForm
from .models import Feedback
from billing.models import Plan


class MainView(TemplateView):
    template_name = 'index.html'


class ContactView(CreateView):
    model = Feedback
    template_name = 'contactus.html'
    form_class = ContactForm
    success_url = reverse_lazy('thankyou')

    def form_valid(self, form):
        form.save()
        return redirect(self.success_url)


class PricingView(TemplateView):
    template_name = 'pricing.html'

    def get_context_data(self, **kwargs):
        ctx = super(PricingView, self).get_context_data(**kwargs)
        ctx['plans'] = Plan.list_available_plans()
        ctx['show_trial_button'] = True
        return ctx


class DomainView(View):

    def get(self, request, *args, **kwargs):
        return HttpResponse('mEOu0wA')

main = MainView.as_view()
