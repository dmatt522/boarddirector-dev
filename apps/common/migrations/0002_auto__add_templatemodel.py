# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TemplateModel'
        db.create_table(u'common_templatemodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.IntegerField')(unique=True, max_length=100)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('html', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'common', ['TemplateModel'])


    def backwards(self, orm):
        # Deleting model 'TemplateModel'
        db.delete_table(u'common_templatemodel')


    models = {
        u'common.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {})
        },
        u'common.templatemodel': {
            'Meta': {'object_name': 'TemplateModel'},
            'html': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'max_length': '100'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['common']