# -*- coding: utf-8 -*-
from django.conf import settings
from django.http import Http404, HttpResponseNotFound
from django.template import loader


def membership(request):
    membership = None
    if 'current_account' in request.session and request.user:
        try:
            membership = request.user.get_membership(request.session['current_account'])
        except Http404:
            return HttpResponseNotFound(loader.get_template('404.html'))
    return {'current_membership': membership}


def trial_period(request):
    return {'trial_period': settings.TRIAL_PERIOD}


def chameleon(request):
    return {'chameleon_enabled': settings.CHAMELEON_ENABLED}
