# -*- coding: utf-8 -*-
import os
import binascii
import hashlib
import random
from PIL import Image
import StringIO

from django.conf import settings
from django.test import TestCase
from django.core.files.uploadedfile import InMemoryUploadedFile


def get_temporary_image(ext='jpg'):
    io = StringIO.StringIO()
    size = (200, 200)
    color = (255, 0, 0, 0)
    image = Image.new("RGBA", size, color)
    image.save(io, format='JPEG')
    name = 'test.{}'.format(ext)
    image_file = InMemoryUploadedFile(io, None, name, 'jpeg', io.len, None)
    image_file.seek(0)
    return image_file


def avatar_upload_to(instance, filename):
    """Generates likely unique image path using md5 hashes"""
    filename, ext = os.path.splitext(filename.lower())
    instance_id_hash = hashlib.md5(str(instance.id)).hexdigest()
    filename_hash = ''.join(random.sample(hashlib.md5(filename.encode('utf-8')).hexdigest(), 8))
    return settings.AVATAR_UPLOAD_ROOT_TEMPLATE.format(instance_id_hash=instance_id_hash,
                                                       filename_hash=filename_hash, ext=ext)


def random_with_n_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return random.randint(range_start, range_end)


def random_hex(length):
    """Generate random hex number returned as unicode. Good for slugs and filenames."""
    return binascii.hexlify(os.urandom(length)).decode()[:length]


def crops_upload_to(instance, filename, crop_name):
    """
    Default function to specify a location to save crops to.

    :param instance: The model instance this crop field belongs to.
    :param filename: The image's filename this crop field operates on.
    :param crop_name: The crop name used when :attr:`CropFieldDescriptor.crop` was
        called.
    """
    filename, ext = os.path.splitext(os.path.split(filename)[-1])
    return settings.AVATAR_CROPS_UPLOAD_ROOT_TEMPLATE.format(filename=filename, crop_name=crop_name, ext=ext)


class BaseTestCase(TestCase):
    fixtures = ['initial_data.json']

    def ajax(self, *args, **kwargs):
        kwargs['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        return self.client.post(*args, **kwargs)

    def ajax_get(self, *args, **kwargs):
        kwargs['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        return self.client.get(*args, **kwargs)

    def create_init_data(self):
        from committees.models import Committee
        from profiles.models import User, Membership
        from accounts.factories import AccountFactory
        from profiles.factories import AdminFactory, UserFactory
        from documents.factories import DocumentFactory
        from meetings.factories import MeetingFactory
        from committees.factories import CommitteeFactory
        self.account = AccountFactory()
        self.admin = AdminFactory(accounts=[self.account])
        UserFactory.create_batch(5, accounts=[self.account])
        CommitteeFactory.create_batch(5)
        for membership in Membership.objects.filter(user__in=User.objects.select_related().exclude(is_superuser=True)):
            membership.committees.add(*random.sample(set(Committee.objects.filter(account_id=membership.account_id)), 2))
        # Set last membership as comittee chairman (comittee_set)
        membership.committee_set.add(*random.sample(set(Committee.objects.filter(account_id=membership.account_id)), 1))
        self.meetings = MeetingFactory.create_batch(2)
        for meeting in self.meetings:
            DocumentFactory.create_batch(2, meeting=meeting)
        self.membership = Membership.objects.filter(role=Membership.ROLES.member, account=self.account)[0]
        self.membership_admin = Membership.objects.filter(role=Membership.ROLES.admin, account=self.account)[0]
        self.user = self.membership.user

    def login_admin(self):
        self.client.login(username=self.admin.email, password='admin')
        self.session = self.client.session
        self.session['current_account'] = self.account
        self.session.save()

    def login_member(self, email=None):
        if email is None:
            email = self.user.email
        self.client.login(username=email, password='member')
        self.session = self.client.session
        self.session['current_account'] = self.account
        self.session.save()


us_timezones = ['US/Alaska',
                'US/Arizona',
                'US/Central',
                'US/Eastern',
                'US/Hawaii',
                'US/Mountain',
                'US/Pacific',
                'UTC']
us_timezones = [tz for tz in us_timezones]
us_timezones_set = set(us_timezones)
