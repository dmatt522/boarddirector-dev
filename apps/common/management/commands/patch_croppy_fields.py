# -*- coding: utf-8 -*-
import os
import re
from StringIO import StringIO
from sys import stdout


from django.core.management.base import BaseCommand


class ImportPatcher(object):

    def __init__(self, filename):
        self._filename = filename

    def __read_source(self):
        with open(self._filename, "r") as f:
            return f.read()

    def has_import(self, import_str):
        """
        Checks for presence of specified import statement at root level (non indented) in the file
        """
        ptrn = "^" + re.sub("[ \t]+", "[ \t]+", import_str)
        content = self.__read_source()
        m = re.search(ptrn, content, re.MULTILINE)
        return (m is not None)

    def add_import(self, import_str):
        """
        Adds specified import statement at top of the file
        """
        content = self.__read_source()
        with open(self._filename, "w+") as outfile:
            outfile.write(import_str + os.linesep)
            outfile.write(content)


class ClassPatcher(object):

    def __init__(self, filename):
        self._filename = filename

    @staticmethod
    def whitespace_weight(ws):
        # Returns python 2.x weight of the whitespace: space is 1, tab indents to next plural of 8
        w = 0
        for c in ws:
            if c == " ":
                w += 1
            elif c == "\t":
                w = ((w/8) + 1) * 8
            else:
                break
        return w

    @staticmethod
    def def_regex(def_name=None):
        d = (def_name or "\w+").decode('string_escape')
        ptrn = "^(?P<leading_ws>[ \t]*)(?:class|def)[ \t]+" + d + "[ \t]*(?:\([a-zA-Z0-9 \_\*\.\,]*\))?\:"
        return re.compile(ptrn)

    @staticmethod
    def decorator_regex(decorator_name=None):
        d = (decorator_name or "[a-zA-Z0-9\.]+").decode('string_escape')
        ptrn = "^(?P<leading_ws>[ \t]*)\@" + d + "\s*(?:\([a-zA-Z0-9 \,]*\))?"
        return re.compile(ptrn)

    def find_definition(self, def_name, start_line=0, end_line=100000, decorator=None):
        """
        Finds a definition (class, function, method) in the source code.
        :param def_name: definition name
        :param start_line: index of first line to search (zero based, optional)
        :param end_line: index of last line to search (zero based, optional)
        :param decorator: name of the expected decorator on the class/function (optional)
        :return: if successful: dictionary {start: <line nr>, end: <line nr>, indentation: <str>}
                 else: None
        """
        start_decorator_regex = self.decorator_regex(decorator)
        def_regex = self.def_regex(def_name)
        next_def_regex = [self.def_regex(), self.decorator_regex()]
        found_start_decorator = None
        found_start_line = None
        found_end_line = None
        found_indentation = None

        with open(self._filename, "r") as f:
            for line_nr, line in enumerate(f):
                # Skip lines from start of file
                if line_nr < start_line:
                    continue

                # Skip lines at end of file
                elif line_nr > end_line:
                    break

                # Match the start decorator
                elif decorator and found_start_decorator is None:
                    m = start_decorator_regex.match(line)
                    if m:
                        found_start_decorator = line_nr

                # Search for start of definition
                elif found_start_line is None:
                    m = def_regex.match(line)
                    if m:
                        found_start_line = line_nr
                        found_indentation = m.groupdict().get("leading_ws", "")
                        indentation_weight = self.whitespace_weight(found_indentation)

                # Search for end of definition
                else:
                    for r in next_def_regex:
                        m = r.match(line)
                        if m:
                            break
                    if m:
                        if self.whitespace_weight(m.groupdict().get("leading_ws", "")) <= indentation_weight:
                            found_end_line = line_nr-1
                            break  # no need to look further

        if found_start_line is None:
            return None
        else:
            if found_start_decorator is None:
                effective_start_line = found_start_line
            else:
                effective_start_line = found_start_decorator
            return {"start": effective_start_line,
                    "end": found_end_line or line_nr,
                    "indentation": found_indentation}

    def apply_patch(self, patch):
        """
        Replaces segment in code file with the specified patch
        :param patch: dictionary containing (class/method) name of the code to be replaced, optional
                      decorator name for enhanced searching and the new implementation as a string.
        """
        outfile = StringIO()
        with open(self._filename, "r") as infile:
            source_lines = infile.readlines()

        classname = patch.get("class_name")
        if classname:
            m = self.find_definition(classname)
            if not m:
                raise ValueError("Could not find class '%s' in file '%s'" % (classname, self._filename))
            start_line = m["start"]
            end_line = m["end"]
        else:
            start_line = None
            end_line = None

        m = self.find_definition(patch.get("name"), start_line, end_line, patch.get("decorator"))
        if not m:
            raise ValueError("Could not find definition '%s'" % patch.get("name", ""))

        for line in source_lines[:m["start"]]:
            outfile.write(line)
        for line in patch.get("patch", "").splitlines():
            ln = m["indentation"] + line
            outfile.write(ln.rstrip() + os.linesep)
        for line in source_lines[m["end"]:]:
            outfile.write(line)

        with open(self._filename, "w+") as f:
            outfile.seek(0)
            f.write(outfile.read())


class Command(BaseCommand):
    help = 'Apply patch to croppy fields module for use with PostgreSQL database'

    @property
    def additional_import(self):
        return "from json import loads"

    @property
    def patch(self):
        return {
            "class_name": "CropFieldDescriptor",
            "name": "data",
            "decorator": "data.setter",
            "patch": '''@data.setter
def data(self, value):
    """
    Sets the data attribute and generates :attr:`CropFieldFiles`
    for convenience methods and attributes like :attr:`CropFieldFiles.delete`,
    :attr:`CropFieldFiles.url`, :attr:`CropFieldFiles.path`, etc.
    """
    if isinstance(value, basestring):
        try:
            value = loads(value)
        except ValueError:
            # print "%s.data: Unable to parse object from JSON" % self.__class__.__name__
            # print "Failed object: '%s'" % value
            return
    for name, spec in value.iteritems():
        name = self.validate_name(name)
        self._data[name] = spec

        if hasattr(self, name):
            continue

        setattr(self, name, CropFieldFile(
            name, spec,
            self.instance,
            self.field,
            spec['filename']))
'''}

    def handle(self, *args, **options):
        # Get file location
        import croppy.fields
        fn = os.path.abspath(croppy.fields.__file__)
        if os.path.splitext(fn)[1].lower() == ".pyc":
            os.remove(fn)
            fn = fn[:-1]

        # Apply patch 1: import statement
        ip = ImportPatcher(fn)
        if not ip.has_import(self.additional_import):
            ip.add_import(self.additional_import)

        # Apply patch 2: method implementation
        cp = ClassPatcher(fn)
        cp.apply_patch(self.patch)
        stdout.write("Applied patch to file %s\n" % fn)
