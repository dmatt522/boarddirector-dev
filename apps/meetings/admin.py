# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Meeting


class MeetingAdmin(admin.ModelAdmin):
    list_display = ('name', 'start', 'end', 'committee', 'account')

admin.site.register(Meeting, MeetingAdmin)
