# -*- coding: utf-8 -*-
from datetime import datetime
from itertools import groupby
from collections import defaultdict
from icalendar import Calendar, Event, vCalAddress, vText
from six import StringIO

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.query_utils import Q
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

from common.choices import Choices
from common.models import TemplateModel
from dashboard.models import RecentActivity
from documents.models import Document, Folder
from profiles.models import Membership


class MeetingManager(models.Manager):
    # TODO - convert to QuerySet as_manager after Django upgrade
    def for_membership(self, membership, only_own_meetings=False):
        queryset = self.filter(account=membership.account)
        # limit guest to see only his committee meetings
        if membership.role == membership.ROLES.guest or only_own_meetings:
            # A longer form of: committee_ids = membership.committees.all().values_list('id', flat=True)
            # It results in one less join in resulting query, and so better plan
            committee_ids = membership.committees.through.objects.filter(membership=membership).values_list('committee_id', flat=True)
            direct_meetings = membership.meeting_set.through.objects.filter(membership=membership).values_list('meeting_id', flat=True)
            if membership.role == membership.ROLES.guest:
                queryset = queryset.filter(Q(committee__id__in=committee_ids) | Q(id__in=direct_meetings))
            else:
                queryset = queryset.filter(Q(committee_id=None) | Q(committee__id__in=committee_ids) | Q(id__in=direct_meetings))

        return queryset


class Meeting(models.Model):
    STATUSES = Choices(
        (0, 'draft', _('Draft')),
        (1, 'published', _('Published')))

    name = models.CharField(_('name'), max_length=250)
    start = models.DateTimeField(_('from'))
    end = models.DateTimeField(_('to'))
    location = models.CharField(_('location'), max_length=250)
    # committee can be Null if Meeting attendees - All Board Members
    committee = models.ForeignKey('committees.Committee', verbose_name=_('committee'), null=True, blank=True,
                                  related_name='meetings')
    account = models.ForeignKey('accounts.Account', verbose_name=_('account'), related_name='meetings')

    description = models.TextField(verbose_name=_('description'), null=False, blank=False)

    is_event = models.BooleanField(default=False, verbose_name=_('event'), null=False)

    status = models.SmallIntegerField(verbose_name=_('status'), null=False, default=STATUSES.draft)
    last_published = models.DateTimeField(verbose_name=_('last published'), null=True, blank=True)
    last_email_sent = models.DateTimeField(verbose_name=_('last email sent'), null=True, blank=True)

    extra_members = models.ManyToManyField('profiles.Membership', db_table='meetings_meeting_member')

    objects = MeetingManager()

    class Meta:
        ordering = ['-start']

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        url_name = 'events:detail' if self.is_event else 'meetings:detail'
        return reverse(url_name, kwargs={'pk': self.pk, 'url': self.account.url})

    def publish(self):
        self.status = Meeting.STATUSES.published
        self.last_published = datetime.now()

    def unpublish(self):
        self.status = Meeting.STATUSES.draft
        self.last_published = None

    @property
    def cached_docs(self):
        if not hasattr(self, '_cached_docs'):
            cached_docs = defaultdict(list)
            for key, docs in groupby(self.folder.documents.all(), lambda x: x.type):
                cached_docs[key] = list(docs)
            self._cached_docs = cached_docs
        return self._cached_docs

    @property
    def is_published(self):
        return self.status == Meeting.STATUSES.published

    @cached_property
    def creator(self):
        try:
            meeting_type = ContentType.objects.get(app_label="meetings", model="meeting")
            user = RecentActivity.objects.get(
                content_type=meeting_type,
                account=self.account,
                object_id=self.id,
                action_flag=RecentActivity.ADDITION
            ).init_user
            member = user.membership_set.get(account=self.account)
            return member
        except RecentActivity.DoesNotExist:
            pass
        return None

    @cached_property
    def extra_members_list(self):
        """ NOTE: Use only for view - is not reset on extra_members manipulations. """
        return list(self.extra_members.filter(user__is_active=True, is_active=True).select_related('user'))

    def _core_members(self):
        if self.committee is None:
            members = self.account.memberships.exclude(role=Membership.ROLES.guest)
        else:
            members = self.committee.memberships.all()
        # if self.creator is not None:
        #     members = members.exclude(user=self.creator.user)
        return members.filter(user__is_active=True, is_active=True).select_related('user')

    @property
    def members(self):
        result = list(self._core_members())
        for m in result:
            m.is_meeting_member_invited = False

        ids = {m.id for m in result}
        for m in self.extra_members.filter(user__is_active=True, is_active=True):
            if m.id not in ids:
                m.is_meeting_member_invited = True
                result.append(m)

        return result

    def check_user_is_member(self, user):
        if self.extra_members.filter(user=user).exists():
            return True

        return self._core_members().filter(user=user).exists()

    def get_minutes(self):
        if Document.MINUTES in self.cached_docs:
            return self.cached_docs[Document.MINUTES][0]
        return None

    def get_agenda(self):
        if Document.AGENDA in self.cached_docs:
            return self.cached_docs[Document.AGENDA][0]
        return None

    def get_other_docs(self):
        if Document.OTHER in self.cached_docs:
            return self.cached_docs[Document.OTHER]
        return None

    def get_committee_name(self):
        if self.committee:
            return self.committee.name
        else:
            return _('All Board Members')

    def generate_ics(self, user, current_member):
        tz_info = current_member.timezone
        cal = Calendar()
        event = Event()
        event.add('summary', self.name)
        event.add('dtstart', self.start.astimezone(tz_info))
        event.add('dtend', self.end.astimezone(tz_info))
        # event.add('dtstamp', datetime(2005,4,4,0,10,0,tzinfo=pytz.utc))

        organizer = vCalAddress('MAILTO:' + user.email)
        organizer.params['cn'] = vText(user.get_full_name())

        event['organizer'] = organizer
        event['location'] = vText(self.location)
        event['uid'] = user.email
        event.add('priority', 1)

        for member in Membership.objects.filter(committees=self.committee, account=self.account):
            attendee = vCalAddress('MAILTO:' + member.user.email)
            attendee.params['cn'] = vText(member.user.get_full_name())
            event.add('attendee', attendee, encode=0)
        cal.add_component(event)
        return cal.to_ical()

    def send_details_email(self, save=True, template_name=TemplateModel.MEETING, exclude_user=None):
        ctx_dict = {
            'meeting': self,
            'site': Site.objects.get_current(),
            'protocol': settings.SSL_ON and 'https' or 'http',
            'link': self.get_absolute_url(),
        }

        for member in self.members:
            if exclude_user and member.user.id == exclude_user.id:
                continue

            ctx_dict['recipient'] = member

            tmpl = TemplateModel.objects.get(name=template_name)
            subject = tmpl.title or self.account.name  # fixme: which one?
            message = tmpl.generate(ctx_dict)

            mail = EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL, [member.user.email])
            mail.content_subtype = "html"
            try:
                for doc in self.folder.documents.all():
                    mail.attach(doc.name, doc.file.read())
            except Folder.DoesNotExist:
                pass

            ics = self.generate_ics(self.creator.user, member)
            f = StringIO()
            f.name = 'invite.ics'
            f.write(ics)
            mail.attach(f.name, f.getvalue(), 'text/calendar')

            mail.send()
            f.close()

        self.last_email_sent = datetime.now()
        if save:
            self.save()


class MeetingAttendance(models.Model):
    meeting = models.ForeignKey('meetings.Meeting', verbose_name=_('meeting'), null=False, related_name='attendance')
    user = models.ForeignKey('profiles.User', verbose_name=_('user'), null=False, related_name='attendance')
    present = models.BooleanField(_('present'))
