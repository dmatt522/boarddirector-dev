# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Committee
from profiles.models import Membership


class CommitteeAddForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CommitteeAddForm, self).__init__(*args, **kwargs)
        account_users = Membership.objects.filter(account=kwargs['initial']['account'], is_active=True).exclude(role=Membership.ROLES.assistant)
        self.fields['summary'].label = _('Add Summary')
        if self.instance.pk:
            initial = Membership.objects.filter(committees=self.instance)
        else:
            initial = []
        self.fields['chairman'] = forms.ModelMultipleChoiceField(queryset=account_users,
                                                                 widget=forms.SelectMultiple(attrs={'class': 'multiple'}),
                                                                 initial=initial,
                                                                 required=True,
                                                                 label=_('Add Chairman'))
        self.fields['members'] = forms.ModelMultipleChoiceField(queryset=account_users,
                                                                widget=forms.SelectMultiple(attrs={'class': 'multiple'}),
                                                                initial=initial,
                                                                required=True,
                                                                label=_('Add Members'))
        self.fields['uploaded'] = forms.IntegerField(widget=forms.HiddenInput(), required=False)
        self.fields.keyOrder = ['name', 'chairman', 'members', 'summary', 'description']

    class Meta:
        model = Committee
        exclude = ['account']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'txt title', 'placeholder': _('Untitled Committee')}),
            'description': forms.Textarea(attrs={'class': 'kendo_editor'}),
            'summary': forms.Textarea(attrs={'class': 'k-textbox'})
        }
