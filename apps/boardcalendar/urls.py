# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from .views import CalendarView

urlpatterns = patterns(
    '',
    url(r'^$', CalendarView.as_view(), name='list'),
)
