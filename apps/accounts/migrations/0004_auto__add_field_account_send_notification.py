# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Account.send_notification'
        db.add_column(u'accounts_account', 'send_notification',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Account.send_notification'
        db.delete_column(u'accounts_account', 'send_notification')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'date_cancel': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Plan']", 'null': 'True'}),
            'send_notification': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stripe_customer_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'total_storage': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['accounts']