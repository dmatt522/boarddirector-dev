# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    depends_on = (
        ("billing", "0001_initial.py"),
    )

    def forwards(self, orm):
        # Adding model 'Account'
        db.create_table(u'accounts_account', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('url', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=25)),
            ('plan', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['billing.Plan'], null=True)),
            ('total_storage', self.gf('django.db.models.fields.BigIntegerField')(default=0)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('date_cancel', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('stripe_customer_id', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'accounts', ['Account'])


    def backwards(self, orm):
        # Deleting model 'Account'
        db.delete_table(u'accounts_account')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'date_cancel': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Plan']", 'null': 'True'}),
            'stripe_customer_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'total_storage': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '25'})
        },
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['accounts']