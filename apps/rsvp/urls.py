# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from .views import RsvpUpdateView


urlpatterns = patterns(
    'rsvp.views',
    url(r'^/?$', RsvpUpdateView.as_view(), name='update'),
)
