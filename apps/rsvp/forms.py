# -*- coding: utf-8 -*-
from django import forms

from .models import RsvpResponse


class RsvpResponseForm(forms.ModelForm):
    class Meta:
        model = RsvpResponse
        fields = ('meeting', 'user', 'response')
        widgets = {}
