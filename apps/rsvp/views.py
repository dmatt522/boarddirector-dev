# -*- coding: utf-8 -*-
from django.core.exceptions import PermissionDenied
from django.views.generic import UpdateView

from common.mixins import AjaxableResponseMixin, LoginRequiredMixin
from meetings.models import Meeting
from .models import RsvpResponse


class RsvpUpdateView(LoginRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = RsvpResponse

    def post(self, request, *args, **kwargs):
        """
        Handles AJAX calls on RsvpResponse objects.

        :param request:
        - request.path: /<account>/rsvp/,
        - request.POST: <QueryDict: {u'meeting': [u'<meeting pk>'],
                                     u'response': [u'Accept|Decline|Tentative']}>,

        If request.POST.response is None or missing, this funnction will retrieve the current status.
        Else it will set/update the new value.

        Security notes:
        - relies on django's CSRF middleware to prevent one user from poking into another user's session
        - relies on django's session middleware to cross-check the requested URL against the logged in user
        - implements check for meeting membership
        """
        try:
            # Check for manipulated request URLs (i.e. /<not this session user's account>/rsvp/)
            account = request.session['current_account']
            request_path_account = request.path.lstrip("/").split("/")[0]
            if account.name != request_path_account:
                raise PermissionDenied()

            # Check if the current user is on the meeting's membership
            meeting = Meeting.objects.get(pk=int(request.POST["meeting"]))
            if not meeting.check_user_is_member(request.user):
                raise PermissionDenied()

            response = request.POST.get("response")

            # Create or update the RSVP response
            if response:
                rsvp = RsvpResponse.objects.create(meeting=meeting, user=request.user)
                rsvp.response = rsvp.response_from_string(response)
                rsvp.save()

            # Query current RSVP response
            else:
                try:
                    response = RsvpResponse.objects.filter(meeting=meeting, user=request.user).latest("timestamp").get_response_display()
                except RsvpResponse.DoesNotExist:
                    response = None

            return self.render_to_json_response({'meeting': meeting.pk,
                                                 'account': account.pk,
                                                 'response': response})

        except PermissionDenied:
            return self.render_to_json_response({'error': 'Permission denied'}, status=403)
        except Meeting.DoesNotExist:
            return self.render_to_json_response({'error': 'Meeting not found'}, status=404)
        except (KeyError, ValueError):
            return self.render_to_json_response({'error': 'Unknown value'}, status=400)
