# -*- coding: utf-8 -*-
import calendar
import stripe

from django.conf import settings
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from accounts.models import Account
from billing.models import BillingSettings


class PlanChangeForm(forms.ModelForm):

    class Meta:
        model = Account
        fields = ['plan']
        widgets = {
            'plan': forms.RadioSelect()
        }


class BillingSettingsEditForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(BillingSettingsEditForm, self).__init__(*args, **kwargs)
        initial_card = ''
        if self.instance.pk:
            card_number = self.instance.card_number
            if card_number:
                initial_card = '-'.join(card_number[x:x + 4] for x in range(0, len(card_number), 4))
        self.fields['creditcardnumber'] = forms.CharField(label=_('Credit Card Number'), initial=initial_card,
                                                          widget=forms.TextInput(
                                                              attrs={'placeholder': _('Enter your credit card number'),
                                                                     'class': 'txt'}
                                                          ), required=True)
        initial_monthpicker = '{:%B %Y}'.format(timezone.now())
        if self.instance.pk and self.instance.expiration_month:
            initial_monthpicker = '{} {}'.format(calendar.month_name[self.instance.expiration_month],
                                                 self.instance.expiration_year)
        self.fields['monthpicker'] = forms.CharField(label=_('Expiration'), required=True, initial=initial_monthpicker)

    class Meta:
        model = BillingSettings
        fields = ['address', 'city', 'state', 'zip', 'country', 'mail', 'name', 'cvv', 'discount']
        widgets = {
            'address': forms.TextInput(attrs={'class': 'txt', 'required': 'required'}),
            'city': forms.TextInput(attrs={'class': 'txt', 'required': 'required'}),
            'state': forms.TextInput(attrs={'class': 'txt', 'required': 'required'}),
            'zip': forms.TextInput(attrs={'class': 'txt', 'required': 'required'}),
            'country': forms.TextInput(attrs={'class': 'txt', 'required': 'required'}),
            'mail': forms.TextInput(attrs={'class': 'txt', 'required': 'required'}),
            'name': forms.TextInput(attrs={'class': 'txt', 'required': 'required'}),
            'cvv': forms.TextInput(attrs={'class': 'txt', 'placeholder': _('e.g.123')}),
            'discount': forms.TextInput(attrs={'class': 'txt', 'placeholder': _('Enter your promocode here')})
        }

    def clean_monthpicker(self):
        monthpicker = self.cleaned_data['monthpicker']
        try:
            list(calendar.month_name).index(monthpicker.split()[0])
        except:
            raise forms.ValidationError(_('Invalid format.'))
        return monthpicker

    def clean_discount(self):
        discount = self.cleaned_data.get('discount', '').strip()
        if discount:
            try:
                stripe.api_key = settings.STRIPE_SECRET_KEY
                stripe.Coupon.retrieve(discount)
            except stripe.InvalidRequestError as e:
                raise forms.ValidationError(e)
        return discount


class BillingCycleUpdateForm(forms.ModelForm):

    class Meta:
        model = BillingSettings
        fields = ['cycle']
        widgets = {
            'cycle': forms.RadioSelect(attrs={'id': 'bill'})
        }


class BillingAddressEditForm(forms.ModelForm):

    class Meta:
        model = BillingSettings
        fields = ['address', 'city', 'state', 'zip', 'country', 'mail', 'name']
        widgets = {
            'address': forms.TextInput(attrs={'class': 'txt'}),
            'city': forms.TextInput(attrs={'class': 'txt'}),
            'state': forms.TextInput(attrs={'class': 'txt'}),
            'zip': forms.TextInput(attrs={'class': 'txt'}),
            'country': forms.TextInput(attrs={'class': 'txt'}),
            'mail': forms.TextInput(attrs={'class': 'txt'}),
            'name': forms.TextInput(attrs={'class': 'txt'}),
        }
