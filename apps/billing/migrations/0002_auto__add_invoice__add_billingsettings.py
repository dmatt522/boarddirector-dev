# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    depends_on = (
        ("accounts", "0001_initial.py"),
    )

    def forwards(self, orm):
        # Adding model 'Invoice'
        db.create_table(u'billing_invoice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('payment', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('status', self.gf('django.db.models.fields.PositiveIntegerField')(default=2)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(related_name='invoices', to=orm['accounts.Account'])),
            ('payed_period_end', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'billing', ['Invoice'])

        # Adding model 'BillingSettings'
        db.create_table(u'billing_billingsettings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('account', self.gf('django.db.models.fields.related.OneToOneField')(related_name='billing_settings', unique=True, to=orm['accounts.Account'])),
            ('card_number', self.gf('django.db.models.fields.CharField')(max_length=16, null=True)),
            ('expiration_month', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('expiration_year', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('cvv', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True)),
            ('cycle', self.gf('django.db.models.fields.PositiveIntegerField')(default=1)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('zip', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('mail', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=70)),
        ))
        db.send_create_signal(u'billing', ['BillingSettings'])


    def backwards(self, orm):
        # Deleting model 'Invoice'
        db.delete_table(u'billing_invoice')

        # Deleting model 'BillingSettings'
        db.delete_table(u'billing_billingsettings')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'date_cancel': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Plan']", 'null': 'True'}),
            'stripe_customer_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'total_storage': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'billing.billingsettings': {
            'Meta': {'object_name': 'BillingSettings'},
            'account': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'billing_settings'", 'unique': 'True', 'to': u"orm['accounts.Account']"}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'card_number': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'cvv': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'cycle': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'expiration_month': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            'expiration_year': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'zip': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'billing.invoice': {
            'Meta': {'object_name': 'Invoice'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'invoices'", 'to': u"orm['accounts.Account']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'payed_period_end': ('django.db.models.fields.DateTimeField', [], {}),
            'payment': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'status': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2'})
        },
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['billing']