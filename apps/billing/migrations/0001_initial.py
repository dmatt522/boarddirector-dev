# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Plan'
        db.create_table(u'billing_plan', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.PositiveIntegerField')(default=2, unique=True)),
            ('max_members', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('max_storage', self.gf('billing.fields.BigIntegerSizeField')()),
            ('month_price', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('year_price', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('stripe_month_plan_id', self.gf('django.db.models.fields.CharField')(max_length=70)),
            ('stripe_year_plan_id', self.gf('django.db.models.fields.CharField')(max_length=70)),
        ))
        db.send_create_signal(u'billing', ['Plan'])


    def backwards(self, orm):
        # Deleting model 'Plan'
        db.delete_table(u'billing_plan')


    models = {
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        }
    }

    complete_apps = ['billing']