# -*- coding: utf-8 -*-
import calendar
import stripe

from django.contrib import messages
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.mail import mail_admins
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404
from django.views.generic.edit import View, UpdateView
from django.utils.translation import ugettext_lazy as _

from .businessrules import ChangePlan
from .forms import PlanChangeForm, BillingSettingsEditForm, BillingAddressEditForm, BillingCycleUpdateForm
from .models import BillingSettings
from accounts.forms import AccountReactivateForm
from accounts.models import Account
from billing.models import Plan
from common.mixins import LoginRequiredMixin, SelectBoardRequiredMixin, AjaxableResponseMixin
from permissions import PERMISSIONS
from permissions.mixins import PermissionMixin
from permissions.shortcuts import has_role_permission


class ChangeBillingCycleView(AjaxableResponseMixin, SelectBoardRequiredMixin, PermissionMixin, View):
    permission = (Account, PERMISSIONS.edit)

    def post(self, request):
        account = request.session['current_account']

        # update stripe subscription
        account._update_subscription()

        BillingSettings.objects.filter(account=account).update(cycle=BillingSettings.YEAR)
        messages.success(request, _('Billing cycle was changed successfully.'))
        data = {'url': reverse('board_detail', kwargs={'url': account.url})}

        return self.render_to_json_response(data)


class AccountObjectMixin(object):
    def get_object(self, queryset=None):
        # can't use PermissionMixin because there's no current_account when reactivating
        if 'account_id' in self.request.GET:
            form = AccountReactivateForm(self.request.GET)
            if form.is_valid():
                account = get_object_or_404(self.request.user.accounts.all(), id=form.cleaned_data['account_id'])
            else:
                raise PermissionDenied()
        elif 'current_account' in self.request.session:
            account = self.request.session['current_account']
        # handle permissions manually
        membership = self.request.user.get_membership(account=account)
        if not has_role_permission(membership, Account, PERMISSIONS.edit):
            raise PermissionDenied()
        return account


class BillingSettingsUpdateView(LoginRequiredMixin, AccountObjectMixin, UpdateView):
    form_class = BillingSettingsEditForm
    template_name = 'billing/billing_settings.html'
    context_object_name = 'billing'

    def get_object(self, queryset=None):
        account = super(BillingSettingsUpdateView, self).get_object()
        return BillingSettings.objects.get(account=account)

    def get_context_data(self, **kwargs):
        context = super(BillingSettingsUpdateView, self).get_context_data(**kwargs)
        context['cycle_form'] = BillingCycleUpdateForm(instance=self.get_object())
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)

        # save cycle
        cycle_form = BillingCycleUpdateForm(self.request.POST)
        if cycle_form.is_valid():
            self.object.cycle = cycle_form.cleaned_data['cycle']

        monthpicker = form.cleaned_data['monthpicker'].split()
        self.object.expiration_month = list(calendar.month_name).index(monthpicker[0])
        self.object.expiration_year = monthpicker[1]
        self.object.card_number = form.cleaned_data['creditcardnumber'].replace("-", "")
        self.object.save()

        if (form.has_changed() and not self.object.account.is_trial()) or (not form.has_changed() and not
                                                                           self.object.account.is_active):
            resp = self.object.account._create_subscription()
            if resp['status'] == 'error':
                messages.error(self.request, resp['msg'])
                return super(BillingSettingsUpdateView, self).form_invalid(form)
            elif resp['status'] == 'success':
                self.request.session['current_account'] = resp['account']
        elif self.object.account.is_active and not self.object.account.is_trial():
            # update stripe subscription
            self.object.account._update_subscription()

        if not self.object.account.is_active:
            self.object.account.is_active = True
            self.object.account.save()

        messages.success(self.request, _('Billing Settings were changed successfully.'))
        return super(BillingSettingsUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('billing:update_settings')


class ChangePlanView(LoginRequiredMixin, AccountObjectMixin, UpdateView):
    form_class = PlanChangeForm
    template_name = 'billing/pricing.html'
    context_object_name = 'account'

    def form_valid(self, form):
        self.object = form.save()
        self.request.session['current_account'] = self.object

        # update stripe subscription
        self.object._update_subscription()

        messages.success(self.request, _('Plan was changed successfully.'))
        return super(ChangePlanView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ChangePlanView, self).get_context_data(**kwargs)
        account = context.get('account')
        if account:
            current_plan = Account.objects.get(pk=account.pk).plan
            current_plan_id = current_plan and current_plan.pk or None
        else:
            current_plan_id = None
        context['plans'] = Plan.list_available_plans()
        context['current_plan_id'] = current_plan_id
        context['selectable_plan_ids'] = ChangePlan.allowed_plans(current_plan_id)
        context['show_change_plan_form'] = True
        return context

    def get_success_url(self):
        return reverse('billing:change_plan')


class BillingAddressUpdateView(SelectBoardRequiredMixin, PermissionMixin, UpdateView):
    permission = (Account, PERMISSIONS.edit)
    model = BillingSettings
    form_class = BillingAddressEditForm
    template_name = 'billing/billing_address.html'
    context_object_name = 'billing'

    def get_object(self, queryset=None):
        return BillingSettings.objects.get(account=self.request.session['current_account'])

    def form_valid(self, form):
        self.object = form.save()

        # update stripe customer email
        if self.object.account.stripe_customer_id:
            stripe.api_key = settings.STRIPE_SECRET_KEY

            try:
                c = stripe.Customer.retrieve(self.object.account.stripe_customer_id)
                c.email = self.object.mail
                c.save()
            except stripe.InvalidRequestError as e:
                mail_admins('Stripe API Error', e.json_body['error']['message'], fail_silently=True)

        messages.success(self.request, _('Billing details were changed successfully.'))
        return super(BillingAddressUpdateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('billing:update_settings')
