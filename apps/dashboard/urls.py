# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from .views import DashboardView, GettingStartedView, ActivitiesView


urlpatterns = patterns(
    'dashboard.views',
    url(r'^$', DashboardView.as_view(), name='dashboard'),
    url(r'^getting-started/$', GettingStartedView.as_view(), name='getting_started'),
    url(r'^activity/$', ActivitiesView.as_view(), name='activity'),
)
