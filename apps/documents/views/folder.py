# -*- coding: utf-8 -*-
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, FormView, RedirectView
from django.views.generic.edit import View

from committees.models import Committee
from common.mixins import (ActiveTabMixin, AjaxableResponseMixin, RecentActivityMixin, SelectBoardRequiredMixin,
                           CurrentAccountMixin, LoginRequiredMixin)
from dashboard.models import RecentActivity
from documents.forms import DocumentAddForm, FolderAddForm, FolderEditForm, FolderMoveForm
from documents.models import Folder, Document, AuditTrail
from permissions import PERMISSIONS
from permissions.mixins import PermissionMixin
from permissions.shortcuts import has_object_permission, filter_by_permission

ALL_COMMITTEES = -1


class FolderQuerysetMixin(object):
    def get_queryset(self):
        account = self.request.session['current_account']
        queryset = Folder.objects.filter(account=account)
        return queryset


class DocumentAddView(ActiveTabMixin, AjaxableResponseMixin, CurrentAccountMixin, RecentActivityMixin,
                      SelectBoardRequiredMixin, FolderQuerysetMixin, PermissionMixin, FormView):
    permission = (Folder, PERMISSIONS.add)
    form_class = DocumentAddForm
    template_name = 'documents/document_add.html'
    active_tab = 'folders'

    def and_permission(self, account, membership):
        folder = self.get_permission_object()
        self.folder = folder
        return folder.can_add_files

    def get_permission_object(self):
        return get_object_or_404(self.get_queryset(), slug=self.kwargs['slug'])

    def form_valid(self, form):
        self.object = self.request.session['current_account']
        if form.cleaned_data.get('notify_group'):
            notify_group = int(form.cleaned_data.get('notify_group'))
        else:
            notify_group = None
        notify_me = True if form.cleaned_data.get('notify_me') == 'true' else False
        documents = []
        if form.cleaned_data['uploaded_file']:
            docs = form.cleaned_data['uploaded_file'].split(',')
            for d in docs:
                try:
                    doc = Document.objects.get(id=d)
                    doc.account = self.object
                    doc.folder = self.folder
                    doc.created_at = timezone.now()
                    doc.save()
                    documents.append(doc)
                except (Document.DoesNotExist, Folder.DoesNotExist):
                    pass
        if self.request.POST.get('new_document'):
            self.save_recent_activity(action_flag=RecentActivity.ADDITION)
            self.get_success_add_message()
        else:
            self.save_recent_activity(action_flag=RecentActivity.CHANGE)
            self.get_success_change_message()
            if self.request.session['current_account'].send_notification:
                user_membership = self.request.user.get_membership(self.object)
                if notify_group:
                    if notify_group != ALL_COMMITTEES:
                        members = list(Committee.objects.get(id=notify_group).memberships.all().
                                       exclude(Q(user=user_membership.user) | Q(user__is_active=False)).
                                       select_related('User'))
                    else:
                        members = list(self.object.memberships.all().
                                       exclude(Q(user=user_membership.user) | Q(user__is_active=False)).
                                       select_related('User'))
                    if notify_me:
                        members.append(user_membership)
                    for doc in documents:
                        doc.send_notification_email(members)

        return super(DocumentAddView, self).form_valid(form)

    def get_context_data(self, *args, **kwargs):
        context = super(DocumentAddView, self).get_context_data(*args, **kwargs)
        context['folder'] = self.folder
        return context

    def get_success_add_message(self):
        messages.success(self.request, _('Documents were added successfully.'))

    def get_success_change_message(self):
        messages.success(self.request, _('Documents were edit successfully.'))

    def get_success_url(self):
        return reverse('folders:folder_detail', kwargs={'url': self.request.session['current_account'].url,
                                                        'slug': self.kwargs['slug']})


class FolderContextMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super(FolderContextMixin, self).get_context_data(*args, **kwargs)
        folder = self.get_object()
        # Folder forms
        context['folder_add_form'] = FolderAddForm()
        context['folder_edit_form'] = FolderEditForm()
        context['folder_move_form'] = FolderMoveForm()
        # Ordered items (files & folders)
        membership = self.request.user.get_membership(self.request.session['current_account'])
        items = filter_by_permission(folder.children.all(), membership, PERMISSIONS.view)
        items += filter_by_permission(folder.documents.all(), membership, PERMISSIONS.view)
        # Order items
        ordering = self.request.GET.get('o', '').lower()
        reverse = ordering.startswith('-')
        if 'name' in ordering:
            items = sorted(items, key=lambda i: i.name, reverse=reverse)
        elif 'date' in ordering:
            items = sorted(items, key=lambda i: i.sort_date, reverse=reverse)
        # append members private folder
        if folder.is_account_root:
            try:
                private_folder = membership.private_folder
            except Folder.DoesNotExist:
                private_folder = Folder.objects.create_membership_folder(membership)
            items.append(private_folder)
        # Set helper attribute is_folder
        for item in items:
            item.is_file = 'Document' in item.__class__.__name__
            item.is_folder = 'Folder' in item.__class__.__name__
        context['items'] = items
        context['ordering'] = ordering
        qs = folder.get_ancestors(include_self=True).filter(membership_id=membership.id)
        membership_ancestor = qs[0] if len(qs) > 0 else None
        context['membership_ancestor'] = membership_ancestor
        return context


class FolderDetailView(ActiveTabMixin, SelectBoardRequiredMixin,
                       FolderContextMixin, FolderQuerysetMixin, PermissionMixin, DetailView):
    permission = (Folder, PERMISSIONS.view)
    active_tab = 'folders'
    template_name = 'documents/folder_detail.html'

    def and_permission(self, account, membership):
        # View only explicitly allowed folders (usually RolePermission view means can view all)
        return has_object_permission(membership, self.get_object(), PERMISSIONS.view)

    def dispatch(self, request, *args, **kwargs):
        # Redirect account root folder to RootFolder view (hides slug)
        if 'current_account' in request.session:
            # call get object only if user has selected board
            folder = super(FolderDetailView, self).get_object()
            if folder.is_account_root:
                account = request.session['current_account']
                return redirect('folders:rootfolder_detail', url=account.url)
        return super(FolderDetailView, self).dispatch(request, *args, **kwargs)


class RootFolderDetailView(ActiveTabMixin, SelectBoardRequiredMixin,
                           FolderContextMixin, PermissionMixin, DetailView):
    permission = (Folder, PERMISSIONS.view)
    active_tab = 'folders'
    template_name = 'documents/folder_detail.html'

    def get_object(self, queryset=None):
        return Folder.objects.get_account_root(account=self.request.session['current_account'])


class RootFolderRedirectView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, **kwargs):
        return reverse_lazy('folders:rootfolder_detail', kwargs={'url': self.request.session['current_account'].url})


class FolderAddView(AjaxableResponseMixin, SelectBoardRequiredMixin,
                    FolderQuerysetMixin, PermissionMixin, FormView):
    permission = (Folder, PERMISSIONS.add)
    form_class = FolderAddForm
    template_name = 'documents/folder_detail.html'

    def and_permission(self, account, membership):
        parent = self.get_permission_object()
        self.parent = parent
        return parent.can_add_folders

    def get_permission_object(self):
        return get_object_or_404(self.get_queryset(), slug=self.kwargs['slug'])

    def form_valid(self, form):
        folder = form.save(commit=False)
        folder.parent = self.parent
        folder.account = self.request.session['current_account']
        folder.user = self.request.user
        folder.save()
        self.object = folder  # needed for AjaxableResponseMixin
        return super(FolderAddView, self).form_valid(form)

    def get_success_url(self):
        # not used in folders.js
        return self.object.get_absolute_url()


class FolderEditView(AjaxableResponseMixin, SelectBoardRequiredMixin,
                     FolderQuerysetMixin, PermissionMixin, FormView):
    permission = (Folder, PERMISSIONS.edit)
    form_class = FolderEditForm
    template_name = 'documents/folder_detail.html'

    def and_permission(self, account, membership):
        folder = self.get_permission_object()
        self.object = folder
        return not folder.protected

    def get_permission_object(self):
        return get_object_or_404(self.get_queryset(), slug=self.kwargs['slug'])

    def get_form_kwargs(self):
        kwargs = super(FolderEditView, self).get_form_kwargs()
        kwargs['instance'] = self.object
        return kwargs

    def form_valid(self, form):
        folder = form.save(commit=False)
        folder.user = self.request.user
        folder.save()
        self.object = folder  # needed for AjaxableResponseMixin
        return super(FolderEditView, self).form_valid(form)

    def get_success_url(self):
        return self.object.get_absolute_url()


class FolderDeleteView(AjaxableResponseMixin, SelectBoardRequiredMixin,
                       FolderQuerysetMixin, PermissionMixin, View):
    permission = (Folder, PERMISSIONS.delete)

    def and_permission(self, account, membership):
        folder = self.get_permission_object()
        self.object = folder
        return not folder.protected

    def get_permission_object(self):
        return get_object_or_404(self.get_queryset(), slug=self.kwargs['slug'])

    def delete_folder(self, folder):
        if folder.protected:
            raise PermissionDenied()
        # Recursion for subfolders
        for subfolder in folder.children.all():
            self.delete_folder(subfolder)
        # Delete documents
        for document in folder.documents.all():
            # create AuditTrail from deleted document
            AuditTrail.objects.create(
                name=document.name,
                file=document.file,
                type=document.type,
                user_id=self.request.user.id,
                change_type=AuditTrail.DELETED,
                latest_version=document.id,
                created_at=document.created_at,
            )
            RecentActivity.objects.filter(
                object_id=document.id,
                content_type_id=ContentType.objects.get_for_model(document)
            ).delete()
            document.delete()
        # Delete folder
        folder.delete()

    def post(self, request, *args, **kwargs):
        self.delete_folder(self.object)
        return self.render_to_json_response({})


class FolderMoveView(AjaxableResponseMixin, FolderContextMixin, FolderQuerysetMixin,
                     SelectBoardRequiredMixin, PermissionMixin, View):
    permission = (Folder, PERMISSIONS.edit)

    def and_permission(self, account, membership):
        folder = self.get_permission_object()
        self.object = folder
        target = get_object_or_404(self.get_queryset(), slug=self.request.POST.get('target_slug'))
        return not folder.protected and target.can_add_folders and has_object_permission(membership, target, PERMISSIONS.add)

    def get_permission_object(self):
        return get_object_or_404(self.get_queryset(), slug=self.kwargs['slug'])

    def post(self, request, *args, **kwargs):
        folder = self.object
        form = FolderMoveForm(request.POST)
        if form.is_valid():
            target = get_object_or_404(self.get_queryset(), slug=form.cleaned_data['target_slug'])
            folder.parent = target
            folder.save()
            return self.render_to_json_response({'result': 'ok'})
        else:
            # QUESTION: Is there already converter ErrorDict -> Dict[String, String]?
            return self.render_to_json_response({'result': 'failed', 'errors': 'TODO'})
