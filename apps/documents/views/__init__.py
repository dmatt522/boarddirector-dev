# -*- coding: utf-8 -*-
from .document import (DocumentAjaxCreateView, DocumentAjaxDeleteView,
                       DocumentDownloadView, DocumentRevisionDownloadView, DocumentSendView)
from .folder import (RootFolderRedirectView, RootFolderDetailView, FolderDetailView, DocumentAddView,
                     FolderAddView, FolderEditView, FolderDeleteView, FolderMoveView)
from .folder_lookup import FolderLookupView
