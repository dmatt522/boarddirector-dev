# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Document, AuditTrail, Folder


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ['file', ]


class DocumentDeleteForm(forms.Form):
    document_id = forms.IntegerField(min_value=1)
    action = forms.CharField(required=False)
    change_type = forms.IntegerField(required=False)

    def clean(self):
        data = self.cleaned_data
        data['change_type'] = AuditTrail.DELETED
        if data['action'] == 'update':
            data['change_type'] = AuditTrail.UPDATED
        return data


class DocumentAddForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DocumentAddForm, self).__init__(*args, **kwargs)
        self.fields['file'] = forms.FileField(label='', required=False)
        self.fields['uploaded_file'] = forms.CharField(widget=forms.HiddenInput(), required=False)
        self.fields['notify_group'] = forms.CharField(widget=forms.HiddenInput(), required=False)
        self.fields['notify_me'] = forms.CharField(widget=forms.HiddenInput(), required=False)
        self.fields.keyOrder = ['file', 'uploaded_file', 'notify_group', 'notify_me']

    class Meta:
        model = Document
        fields = []

    def clean_uploaded_file(self):
        data = self.cleaned_data['uploaded_file']
        if not data:
            raise forms.ValidationError(_('You have not added any documents.'))
        return data


class MessageForm(forms.Form):
    subject = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'txt', 'placeholder': _('Subject')}), max_length=255)
    body = forms.CharField(widget=forms.Textarea(attrs={'class': 'txt kendo_editor'}))


class FolderAddForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'add'
        super(FolderAddForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Folder
        fields = ['name']


class FolderEditForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        kwargs['prefix'] = 'edit'
        super(FolderEditForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Folder
        fields = ['name']


class FolderMoveForm(forms.Form):
    target_slug = forms.SlugField()
