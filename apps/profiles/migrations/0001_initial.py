# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    depends_on = (
        ("committees", "0001_initial.py"),
    )

    def forwards(self, orm):
        # Adding model 'User'
        db.create_table(u'profiles_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
            ('is_staff', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_superuser', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
        ))
        db.send_create_signal(u'profiles', ['User'])

        # Adding model 'Membership'
        db.create_table(u'profiles_membership', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profiles.User'])),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(related_name='memberships', to=orm['accounts.Account'])),
            ('date_joined_board', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now)),
            ('role', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
            ('timezone', self.gf('timezone_field.fields.TimeZoneField')(default='America/Chicago')),
            ('avatar', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('crops', self.gf('croppy.fields.CropField')(default={}, image_field='avatar')),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('term_start', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('term_expires', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('intro', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('bio', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('linkedin', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal(u'profiles', ['Membership'])

        # Adding unique constraint on 'Membership', fields ['user', 'account']
        db.create_unique(u'profiles_membership', ['user_id', 'account_id'])

        # Adding M2M table for field committees on 'Membership'
        m2m_table_name = db.shorten_name(u'profiles_membership_committees')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('membership', models.ForeignKey(orm[u'profiles.membership'], null=False)),
            ('committee', models.ForeignKey(orm[u'committees.committee'], null=False))
        ))
        db.create_unique(m2m_table_name, ['membership_id', 'committee_id'])

        # Adding model 'TemporaryUserPassword'
        db.create_table(u'profiles_temporaryuserpassword', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='tmppswd', unique=True, to=orm['profiles.User'])),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'profiles', ['TemporaryUserPassword'])


    def backwards(self, orm):
        # Removing unique constraint on 'Membership', fields ['user', 'account']
        db.delete_unique(u'profiles_membership', ['user_id', 'account_id'])

        # Deleting model 'User'
        db.delete_table(u'profiles_user')

        # Deleting model 'Membership'
        db.delete_table(u'profiles_membership')

        # Removing M2M table for field committees on 'Membership'
        db.delete_table(db.shorten_name(u'profiles_membership_committees'))

        # Deleting model 'TemporaryUserPassword'
        db.delete_table(u'profiles_temporaryuserpassword')


    models = {
        u'accounts.account': {
            'Meta': {'object_name': 'Account'},
            'date_cancel': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'plan': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['billing.Plan']", 'null': 'True'}),
            'stripe_customer_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'total_storage': ('django.db.models.fields.BigIntegerField', [], {'default': '0'}),
            'url': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'billing.plan': {
            'Meta': {'ordering': "['month_price']", 'object_name': 'Plan'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_members': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'max_storage': ('billing.fields.BigIntegerSizeField', [], {}),
            'month_price': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'name': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2', 'unique': 'True'}),
            'stripe_month_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'stripe_year_plan_id': ('django.db.models.fields.CharField', [], {'max_length': '70'}),
            'year_price': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'committees.committee': {
            'Meta': {'ordering': "['name']", 'object_name': 'Committee'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'committees'", 'to': u"orm['accounts.Account']"}),
            'chairman': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profiles.Membership']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'summary': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        u'profiles.membership': {
            'Meta': {'unique_together': "(('user', 'account'),)", 'object_name': 'Membership'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'memberships'", 'to': u"orm['accounts.Account']"}),
            'avatar': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'bio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'committees': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'memberships'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['committees.Committee']"}),
            'crops': ('croppy.fields.CropField', [], {'default': '{}', 'image_field': "'avatar'"}),
            'date_joined_board': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'intro': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'linkedin': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'role': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'term_expires': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'term_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'timezone': ('timezone_field.fields.TimeZoneField', [], {'default': "'America/Chicago'"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profiles.User']"})
        },
        u'profiles.temporaryuserpassword': {
            'Meta': {'object_name': 'TemporaryUserPassword'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'tmppswd'", 'unique': 'True', 'to': u"orm['profiles.User']"})
        },
        u'profiles.user': {
            'Meta': {'object_name': 'User'},
            'accounts': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'users'", 'null': 'True', 'through': u"orm['profiles.Membership']", 'to': u"orm['accounts.Account']"}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['profiles']