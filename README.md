# boarddirector.co #

## Versions ##

### 0.1.0 (29-Oct-2015) ###
Based on the software as it wat migrated to Bitbucket repo on 30-Sep-2015. Adds the following changes:

* Make system robust for missing avatar / profile pictures. Just show the default picture when no picture can be read.
* Fix problem with calendar date localization
* Fabric deployment automation: functions to support configurtion and control of Supervisor, uWSGI and nginx services.

### 0.1.1 (02-Nov-2015) ###
Changes to Fabric deployment automation:

* support for side-by-side deployment of releases; functions to deploy, remove and activate releases.
* support for forward and backward database migrations.

### 0.2.0 (20-Nov-2015) ###
Feature: RSVP:

* added widget to set RSVP response / view current response.
* added table with RSVP per invitee in the meeting organiser's view.
* added expandable div with RSVP response history per invitee.

Changes to Fabric deployment automation:

* improve robustness in handling changes (add 'collectstatic' to deploy function and PIP update to activation function).
* cleanup obsolete code.

### 0.2.1 (23-Nov-2015) ###
Bugfix: board names may contain spaces and HTML escapes

### 0.2.2 (24-Nov-2015) ###
Bugfix: User.get_short_name() and User.get_full_name() to return names rather than parts of the e-mail address. 

### 0.2.3 (29-Feb-2016) ###
Bugfix:

* Problem with croppy, JSON fields and PostgreSQL is now programatically patched

Feature: configurable pricing

* added 2016 plans
* made "pricing" and "change plan" pages render dynamically based on available plans

### 0.3.0 (12-Apr-2016) ###

Feature: PDF viewer

### 0.3.1 (19-Jul-2016) ###

Bugfix: new settings for outgoing e-mails

### 0.3.2 (26-Jul-2016) ###

AddedHotjar site analytics script


### 15-Nov-2016 ###
 * Fixed tests
 * Added customer.io integration

### 30-Nov-2016 ###
 * Lockdown delete and share

## New Deployment Process ##
Documented in confluence [page](http://52.53.222.198:8090/display/WEB/Production+Deployment+process).


## OLD deployment process ##

* Open a terminal
* Activate the boarddirector dev virtualenv
* Navigate to the root directory of the boarddirector project
* type: `$ fab deploy:<version>`
* type: `$ fab activate:<version>`

(where <version> is the release tag to be deployed/activated)

## Code Style ##

* Tab size is 4
* Translate tabs to spaces
* Use Pep8 linter
* Max line length (for this project) is 160
* Ensure newline at eof
* Trim trailing white space
* For e.g. Sublime Text 3 settings
* "tab_size": 4,
* "translate_tabs_to_spaces": true,
* "pep8_max_line_length": 160,
* "ensure_newline_at_eof_on_save": true,
* "trim_trailing_white_space_on_save": true,
* html tab size is 2

