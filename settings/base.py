# -*- coding: utf-8 -*-
import os
import urlparse

try:
    from .env import apply_environment
    apply_environment()
except ImportError:
    pass


PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))


def rel(*x):
    return os.path.join(PROJECT_DIR, *x)


os.sys.path.append(rel('apps'))

DOMAIN = os.environ.get('DOMAIN')

DEBUG = False
TEMPLATE_DEBUG = DEBUG

EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'Board Director'
EMAIL_HOST_PASSWORD = '16y0NzdoNDKIUr8SZh6dqA'
EMAIL_USE_TLS = True

ADMINS = (
    ('Barrick Michael', 'michaelbarrick@boarddirector.co'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'boarddocuments_db',
        'USER': 'ubuntu',
    }
}

if 'DATABASE_URL' in os.environ:
    # Parse database configuration from $DATABASE_URL
    import dj_database_url
    DATABASES['default'] = dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SSL_ON = os.environ.get('SSL_ON')

SESSION_COOKIE_HTTPONLY = True

if SSL_ON:
    SECURE_PROXY_SSL_HEADER = ('', '')
    SESSION_COOKIE_SECURE = False
    CSRF_COOKIE_SECURE = False
else:
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    EMAIL_CHANGE_USE_HTTPS = True

ALLOWED_HOSTS = [DOMAIN]

TIME_ZONE = 'America/Chicago'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = rel('public', 'media')

MEDIA_URL = '/media/'

STATIC_ROOT = rel('public', 'static')

STATIC_URL = '/static/'

STATICFILES_DIRS = ()

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'dajaxice.finders.DajaxiceFinder',
    'compressor.finders.CompressorFinder',
)

SECRET_KEY = '7&W$5t4rtg53RTEW%$^#4535764yzems*1^scad0-k-q$-(g5d'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'common.middleware.ForceSSLMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'common.middleware.TimezoneMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
)

ROOT_URLCONF = 'common.urls'

WSGI_APPLICATION = 'wsgi.application'

TEMPLATE_DIRS = ()

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'common.context_processors.membership',
    'common.context_processors.trial_period',
    'common.context_processors.chameleon',
    'customer_io.context_processors.access_keys',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.flatpages',

    # libs
    'dajaxice',
    'dajax',
    'south',
    'sorl.thumbnail',
    'change_email',
    'mptt',
    'compressor',

    # apps
    'accounts',
    'billing',
    'boardcalendar',
    'committees',
    'common',
    'documents',
    'meetings',
    'profiles',
    'registration',
    'dashboard',
    'news',
    'rsvp',
    'permissions',
    'customer_io',  # "customerio" is official lib for the customer.io API
)


AVATAR_UPLOAD_ROOT_TEMPLATE = '{instance_id_hash}/{filename_hash}{ext}'
AVATAR_CROPS_UPLOAD_ROOT_TEMPLATE = 'crops/{filename}-{crop_name}{ext}'

USE_S3 = False
if not DEBUG and 'AWS_STORAGE_BUCKET_NAME' in os.environ:
    INSTALLED_APPS += ('storages',)
    AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
    AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
    AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
    STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
    DEFAULT_FILE_STORAGE = 'common.storage_backends.StableS3BotoStorage'
    AVATAR_UPLOAD_ROOT_TEMPLATE = 'uploads/avatars/{instance_id_hash}/{filename_hash}{ext}'
    AVATAR_CROPS_UPLOAD_ROOT_TEMPLATE = 'uploads/avatars/crops/{filename}-{crop_name}{ext}'
    S3_URL = 'https://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
    STATIC_URL = S3_URL
    USE_S3 = True
    AWS_S3_SECURE_URLS = True

# Configure avatare path settings
AVATAR_ROOT = os.path.join(MEDIA_ROOT, 'avatars')
AVATAR_URL = os.path.join(MEDIA_URL, 'avatars/')

DEFAULT_AVATAR = rel('apps', 'common', 'static', 'images', 'default_avatar.png')
DEFAULT_AVATAR_URL = urlparse.urljoin(STATIC_URL, 'images/default_avatar.png')
DEFAULT_LIST_AVATAR = rel('apps', 'common', 'static', 'images', 'default_list_avatar.png')
DEFAULT_LIST_AVATAR_URL = urlparse.urljoin(STATIC_URL, 'images/default_list_avatar.png')

# Configure django registration
DEFAULT_FROM_EMAIL = 'Board Director <info@boarddirector.co>'
SERVER_EMAIL = DEFAULT_FROM_EMAIL

# Stripe keys
STRIPE_PUBLIC_KEY = 'pk_live_spaUCWRSXCRmXAdUlmovwPtl'
STRIPE_SECRET_KEY = 'sk_live_FYTMpnOjyB3ju1N300NA3rDm'

# Customer.io keys
CUSTOMERIO_SITE_ID = 'a2c0d7e4fcad88b325ff'
CUSTOMERIO_API_KEY = '58eb671f05f1a9692a5e'
CUSTOMERIO_ENABLED = True

# Chameleon
CHAMELEON_ENABLED = True

# User settings
AUTH_USER_MODEL = 'profiles.User'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = '/accounts/'

ACCOUNT_ACTIVATION_DAYS = 7
AUTH_USER_EMAIL_UNIQUE = True
TRIAL_PERIOD = 21
DASHBOARD_MEETINGS_COUNT = 65

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

# google maps
GOOGLE_API_KEY = 'cb7ppKHhK7OwAE3Sz3SU32yy'

try:
    from .local import *
except ImportError:
    pass
